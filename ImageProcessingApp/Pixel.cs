﻿using System.Drawing;

namespace ImageProcessingApp
{
    public class Pixel
    {
        public Color Color { get; set; }
        public Point Point { get; set; }
    }
}